# installation FoodPlace-Backend .
## Setup:
- PHP >= 5.6.4
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Imagick Extension
- Laravel 5.2
- Mysql 5.7
- Apache/2.4.18 (Ubuntu)
- Ubuntu 16
- Composer

## Installation
 
1.Clone Project FoodPlace จาก [Here](https://gitlab.com/20SCOOPS/FoodPlace-Backend)
```
git clone -b <branch> <remote_repo>
```
2.ติดตั้ง Composer ในโฟเดอร์ โปรเจค
```
composer install
```
3.สร้างไฟล์ .env โดยใช้ข้อมูลจากไฟล์ .env.example
```
cp .env.example .env
```
แก้ไข ไฟล์  .env ตามดังนี้
![enter image description here](http://www.mx7.com/i/278/6hwdf0.png)

แล้วเพิ่มคำสั่งนี้ ต่อท้ายในไฟล์ .env
```
# gd | imagick
IMAGE_DRIVER=gd

PAYPAL_CLIENT_ID=AQs6vIo-VK12-A8SKebzSTwYi2gHOQGKnqPs53GuBREl0OFFmIutr_hmC7d18ibI1kjBuhB2mpKfXeI9
PAYPAL_CLIENT_SECRET=EAEM_HLR7MRjliaDxUoxplwkb5oePjLT57OgCK7iUb3UiSRKW3IGFLCJNmQrYzbrlNiB2BqoPDfdq7yu
# mode can be sandbox or live
PAYPAL_MODE=sandbox
PAYPAL_LOG_ENABLE=false
# level should be INFO when live
PAYPAL_LOG_LEVEL=DEBUG
PAYPAL_CURRENCY=EUR

PAYPAL_VAULT_MERCHANT_ID=foodplace.local


SOFORT_API_KEY=142310:316910:c1bd0bde06ae069ff477dbdd7f175a7d
SOFORT_CURRENCY=EUR
sofortDebug=true

# PAYMENT_METHOD_PAYPAL = 0
# PAYMENT_METHOD_BANK_TRANSFER = 1
# PAYMENT_METHOD_CREDITCARD = 2
# PAYMENT_METHOD_PAYPAL_SINGLE_PAYMENT = 3
# PAYMENT_METHOD_SOFORT = 4
ALLOW_PAYMENT_TYPE=3,4

JWT_SECRET=20SCOOPS
```

4.Generate Key
```
php artisan key:generate
```
5.ใช้คำสั่งด้านล่าง เพื่อสร้างโฟล์เดอร์สำหรับเก็บรูปภาพ
```
cd public
mkdir upload
mkdir upload/category
mkdir upload/category/512
mkdir upload/category/image
mkdir upload/category/origin
mkdir upload/category/thumbnail
mkdir upload/product
mkdir upload/product/512
mkdir upload/product/image
mkdir upload/product/origin
mkdir upload/product/thumbnail
mkdir upload/product_gallery
mkdir upload/product_gallery/512
mkdir upload/product_gallery/image
mkdir upload/product_gallery/origin
mkdir upload/product_gallery/thumbnail
mkdir upload/course
mkdir upload/course/512
mkdir upload/course/image
mkdir upload/course/origin
mkdir upload/course/thumbnail
mkdir upload/bot
mkdir upload/bot/512
mkdir upload/bot/image
mkdir upload/bot/origin
mkdir upload/bot/thumbnail
mkdir upload/allergy
mkdir upload/allergy/image
mkdir upload/allergy/25
mkdir upload/allergy/50
mkdir upload/allergyPolicy
mkdir upload/allergyPolicy/image
mkdir upload/allergyPolicy/25
mkdir upload/allergyPolicy/50
mkdir upload/claim
mkdir upload/claim/image
mkdir upload/claim/thumbnail
mkdir pdf
mkdir zip
chmod -R 777 upload
chmod -R 777 pdf
chmod -R 777 zip
```
6.Migrations 
```
php artisan migrate
```
## Run
```
php artisan serve
```
